# Rapport SAE - MITE BOXING
## DHORME - DELESPIERRE - DORMAEL

## Diagramme de séquences :

<img src="sequence.png">

## Difficultés :

- La première version a été faite sans le côté serveur donc nous avons rencontré des difficultés pour adapter le projet en client/serveur. Nous avons dû faire une refonte du projet trois semaines avant la fin.
- Mauvais réglage de notre méthode `render()` qui a rendu le jeu non fluide, ce qui a entraîné une perte de temps importante sur cette méthode pour rendre notre jeu plus fluide.

## Points d'amélioration :

- Le bon fonctionnement de la fonction "rejouer" (sur la branche "louis") existe, mais par manque de temps, nous n'avons pas pu régler les bugs qu'elle contient. (La classe SaveScore existe également et fonctionne, elle n'est juste jamais appelée car "rejouer" ne fonctionne pas)
- Ajout de plus de bonus
- Création de plusieurs parties pour pouvoir jouer avec ses amis
- Réalisation de tests supplémentaires
- Implémentation de différentes trajectoires pour les monstres
- Possibilité de déplacement à la souris
- Affichage du nombre de vies

## Ce dont nous sommes les plus fiers :

- La fluidité du jeu
- La bonne autonomie et répartition des tâches
- La reconstruction du projet en deux semaines
