import { io } from 'socket.io-client';
import Input from './input.js';
import View from './View.js';

export default class JeuView extends View {
	canvas;
	context;
	miteImage;
	marronImage;
	monstreImage;
	bonusImage;
	partie;
	id;
	displayEnd;
	newGame;
	inputs;

	constructor(element, canvas, socket) {
		super(element, socket);
		this.canvas = canvas;
		this.context = this.canvas.getContext('2d');
		this.newGame = true;
	}

	init(partie, id) {
		if (this.newGame) {
			this.partie = partie;
			this.id = id;
			document.addEventListener('keydown', this.keyDown.bind(this));
			document.addEventListener('keyup', this.keyUp.bind(this));
			this.partie;
			this.miteImage = new Image();
			this.miteImage.src = '/res/assets/moth.png';
			this.marronImage = new Image();
			this.marronImage.src = '/res/assets/marron.png';
			this.monstreImage = new Image();
			this.monstreImage.src = '/res/assets/boulesAntiMites.png';
			this.bonusImage = new Image();
			this.bonusImage.src = '/res/assets/vape.png';

			this.miteImage.onload = () =>
				(this.marronImage.onload = () =>
					(this.monstreImage.onload = () =>
						(this.bonusImage.onload = () =>
							requestAnimationFrame(this.render.bind(this)))));
			this.newGame = false;
		} else {
			this.partie = partie;
		}
	}

	render() {
		this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
		this.afficherMites();
		this.renderMarron();
		this.renderMonster();
		this.renderBonus();
		this.renderScore();
		this.renderBonusInfo();
		// if (this.displayEnd) {
		// 	document.querySelector('.game').classList.add('disabled');
		// 	document.querySelector('.gameOver').classList.remove('disabled');
		// } else {
		// 	this.isGameFinished();
		// }
		requestAnimationFrame(this.render.bind(this));
	}

	isGameFinished() {
		if (this.partie.mites.length == 0) {
			alert('fin');
			this.displayEnd = true;
		}
	}

	afficherMites() {
		for (let i = 0; i < this.partie.mites.length; i++) {
			const mite = this.partie.mites[i];
			let x = mite.miteX;
			let y = mite.miteY;
			this.context.drawImage(this.miteImage, x, y);
		}
	}

	getCanvas() {
		return this.canvas;
	}

	renderMarron() {
		if (this.partie.shoots.length != 0) {
			for (let i = 0; i < this.partie.shoots.length; i++) {
				this.context.drawImage(
					this.marronImage,
					this.partie.shoots[i].x,
					this.partie.shoots[i].y
				);
			}
		}
	}

	renderMonster() {
		if (this.partie.monsters.length != 0) {
			for (let i = 0; i < this.partie.monsters.length; i++) {
				this.context.drawImage(
					this.monstreImage,
					this.partie.monsters[i].monstreX,
					this.partie.monsters[i].monstreY
				);
			}
		}
	}

	renderBonus() {
		if (this.partie.bonus.length != 0) {
			for (let i = 0; i < this.partie.bonus.length; i++) {
				this.context.drawImage(
					this.bonusImage,
					this.partie.bonus[i].x,
					this.partie.bonus[i].y
				);
			}
		}
	}

	renderScore() {
		document.querySelector('.scoreVal').innerHTML =
			`  Score : ${this.partie.score}`;
	}

	renderBonusInfo() {
		this.partie.mites.forEach(mite => {
			if (mite.idPlayer == this.id) {
				let res = '';
				if (mite.vitesse > 1) {
					res += '  Speed';
				}
				if (mite.immunity) {
					res += '  Immunity';
				}
				if (res == '') {
					document.querySelector('.bonusInfo').classList.add('disabled');
				} else {
					document.querySelector('.bonusInfo').classList.remove('disabled');
					document.querySelector('.bonusInfo').innerHTML = res;
				}
			}
		});
	}
	/*


    document.querySelector('.replay').addEventListener('click', event => {
        document.querySelector('.game').classList.remove('disabled');
        document.querySelector('.gameOver').classList.add('disabled');
        socket.emit('changeGame');
    });

*/

	keyDown(e) {
		if (e.key === ' ') {
			this.socket.emit('shoots', { user: this.id });
		} else {
			this.socket.emit('keyDown', { key: e.key, user: this.id });
		}
	}

	keyUp(e) {
		this.socket.emit('keyUp', { key: e.key, user: this.id });
	}
}
