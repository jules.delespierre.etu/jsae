import View from './View.js';
import Router from './Router.js';

export default class MenuView extends View {
	constructor(element, canvas, socket) {
		super(element, socket);
		this.canvas = canvas;
		this.setAllEventlistener();
		console.log(this.canvas);
	}

	setAllEventlistener() {
		this.element.querySelectorAll('a').forEach(button => {
			button.addEventListener('click', e => {
				e.preventDefault();
				switch (e.currentTarget.getAttribute('href')) {
					case '/game':
						this.socket.emit(
							'wantToPlay',
							{
								pseudo: sessionStorage.getItem('pseudo'),
								canvasWidth: this.canvas.width,
								canvasHeigth: this.canvas.height,
							},
							callback => {
								sessionStorage.setItem('userId', callback.id);
							}
						);
						break;
					case '/credits':
						break;
					case 'highscores':
						break;
				}
				Router.navigate(e.currentTarget.getAttribute('href'));
			});
		});
	}
}
