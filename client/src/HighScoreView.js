import View from './View.js';

export default class HighScoreView extends View {
	constructor(element, socket) {
		super(element, socket);
		this.init();
	}

	init() {
		this.socket.emit('wantHighScore');
		this.socket.on('highScore', scores => {
			let tri = scores.sort((a, b) => b.score - a.score);
			let result = '<tr><th>Pseudo</th><th>Date</th><th>Score</th></tr>';
			tri.forEach(element => {
				console.log(element);
				result =
					result +
					`<tr><td>${element.pseudo}</td><td>${element.date}</td><td>${element.score}</td></tr>`;
			});
			document.querySelector('table').innerHTML = result;
		});
	}
}
