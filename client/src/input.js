export default class Input {
	input;
	isPressed;

	constructor(input, pressed) {
		this.input = input;
		this.isPressed = pressed;
	}

	setPressed(bool) {
		this.isPressed = bool;
	}
}
