export default class View {
	element;
	socket;

	constructor(element, socket) {
		this.element = element;
		this.socket = socket;
	}

	show() {
		this.element.classList.add('showed');
	}

	hide() {
		this.element.classList.remove('showed');
	}
}
