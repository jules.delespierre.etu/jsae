import { io } from 'socket.io-client';
import Router from './Router.js';
import JeuView from './JeuView.js';
import MenuView from './MenuView.js';
import { CreditView } from './CreditView.js';
import HighScoreView from './HighScoreView.js';

const socket = io();

const pseudo =
	sessionStorage.getItem('pseudo') ||
	window.prompt('veuillez entrer votre nom', '');
sessionStorage.setItem('pseudo', pseudo);

const canvas = document.querySelector('.game');

/*
	Partie du Menu	
*/

const menuView = new MenuView(
	document.querySelector('.MenuPage'),
	canvas,
	socket
);
const gameView = new JeuView(
	document.querySelector('.GamePage'),
	canvas,
	socket
);
const creditView = new CreditView(document.querySelector('.credits'), socket);

const highScoreView = new HighScoreView(
	document.querySelector('.scorePage'),
	socket
);

const routes = [
	{ path: '/', view: menuView },
	{ path: '/game', view: gameView },
	{ path: '/credits', view: creditView },
	{ path: '/highscores', view: highScoreView },
];

Router.routes = routes;

socket.on('startGame', data => {
	const id = sessionStorage.getItem('userId');
	if (id != null) {
		gameView.init(data.partie, parseInt(sessionStorage.getItem('userId')));
	}
});

// chargement de la vue initiale selon l'URL demandée par l'utilisateur.rice (Deep linking)
Router.navigate(window.location.pathname, true);
// gestion des boutons précédent/suivant du navigateur (History API)
window.onpopstate = () => Router.navigate(document.location.pathname, true);

const buttonReplay = document.querySelector('.replay');

buttonReplay.addEventListener('click', e => {
	e.preventDefault();
	console.log('Restart demandé');
	socket.emit('changeGame');
});
