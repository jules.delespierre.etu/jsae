export default class Monstre {
	monstreX;
	monstreY;
	vitesse;

	constructor(maxX, maxY, vitesse) {
		this.monstreX = maxX - 75;
		this.monstreY = Math.floor(Math.random() * maxY);
		this.vitesse = vitesse;
	}

	move() {
		this.monstreX -= this.vitesse;
	}
}
