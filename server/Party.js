import PlayerTab from './PlayerTab.js';
import mite from './mite.js';
import tir from './tir.js';
import monstre from './Monstre.js';
import bonus from './Bonus.js';

export default class Party {
	players; // Liste des players dans le jeu
	mites; // Liste des mites présentes sur le canvas
	shoots; // Liste des tirs présents sur le canvas
	monsters; // Liste des monstres présents sur le canvas
	bonus; // Liste des Bonus présents sur le canvas
	maxPlayer; // Nombre de players max dans la party
	maxX; // X maximal du canvas
	maxY; // Y maximal du canvas
	vitesseMonster; //Vitesse des monstres à changer en fonction de la difficulté
	apparitionMonster; //Fréquence d'apparition des monstres
	io;
	finished; // Indique si la partie est finie (true/false)

	score;

	constructor(maxPlayer, io, canvasWidth, canvasHeight) {
		console.log(canvasHeight);
		this.finished = false;
		this.maxPlayer = maxPlayer;
		this.players = new PlayerTab(maxPlayer);
		this.mites = new Array();
		this.shoots = new Array();
		this.monsters = new Array();
		this.bonus = new Array();
		this.vitesseMonster = 1;
		this.apparitionMonster = 100000;
		this.finished = false;
		this.score = 100;
		this.maxX = canvasWidth;
		this.maxY = canvasHeight;

		// Gestion des fréquences d'intervals
		setInterval(() => {
			this.difficultyUp();
		}, 30000 / 60);
		setInterval(() => {
			this.spawnMonster();
			this.incrementScore();
		}, this.apparitionMonster / 60);
		setInterval(() => {
			this.Gaming();
			io.emit('startGame', { partie: this });
		}, 0);
	}

	isGameFinished() {
		if (this.mites.length == 0) {
			this.finished = true;
		}
	}

	Gaming() {
		this.moveMonster();
		this.collisionHandler();
		this.replaceMite();
		this.tirHandler();
		this.shootTouchMonsters();
		this.miteTouchBonus();
		this.isGameFinished();
	}

	addPlayer(pseudo, mite, shoot) {
		if (this.players.playerArray.length < this.maxPlayer) {
			this.players.addPlayer(pseudo);
			this.mites.push(mite);
			this.shoots.push(shoot);
			return true;
		} else {
			return false;
		}
	}

	incrementScore() {
		this.score++;
	}

	removePlayer(playerId) {
		let cpt = 0;
		let idx = undefined;
		players.playerArray.forEach(element => {
			if (element.id == playerId) {
				idx = cpt;
			}
			cpt++;
		});
		if (idx != undefined) {
			this.players.playerArray.splice(idx, 1);
			this.mites.splice(idx, 1);
			this.shoots.splice(idx, 1);
			return true;
		} else {
			return false;
		}
	}

	collisionHandler() {
		this.mites.forEach(mite => {
			if (mite.immunity == false) {
				this.monsters.forEach(monster => {
					const miteLeft = mite.miteX;
					const miteRight = mite.miteX + 75;
					const miteTop = mite.miteY;
					const miteBottom = mite.miteY + 75;
					const monsterLeft = monster.monstreX;
					const monsterRight = monster.monstreX + 50;
					const monsterTop = monster.monstreY;
					const monsterBottom = monster.monstreY + 50;

					if (
						miteRight >= monsterLeft &&
						miteLeft <= monsterRight &&
						miteBottom >= monsterTop &&
						miteTop <= monsterBottom
					) {
						this.playerIsTouched(mite);
					}
				});
			}
		});
	}

	getPseudoOfAPlayer(playerId) {
		let p;
		this.players.playerArray.forEach(element => {
			if (element.id == playerId) {
				p = element.pseudo;
			}
		});
		return p;
	}

	spawnMonster() {
		this.monsters.push(new monstre(this.maxX, this.maxY, 1));
	}

	getPseudoOfAPlayer(playerId) {
		let p;
		this.players.playerArray.forEach(element => {
			if (element.id == playerId) {
				p = element.pseudo;
			}
		});
		return p;
	}

	changeSettings(settings) {
		this.maxX = settings.width;
		this.maxY = settings.height;
	}

	getPlayer(playerId) {
		this.mites.forEach(element => {
			if (element.id == playerId) {
				return element;
			}
		});
	}

	findMiteById(idPlayer) {
		let mite;
		this.mites.forEach(element => {
			if (element.idPlayer === idPlayer) {
				mite = element;
			}
		});
		return mite;
	}

	moveMonster() {
		if (this.monsters.length != 0) {
			for (let index = 0; index < this.monsters.length; index++) {
				if (this.monsters[index].monstreX <= 0) {
					this.monsters.splice(index, 1);
				} else {
					this.monsters[index].move();
				}
			}
		}
	}

	playerIsTouched(mite) {
		console.log(mite.idPlayer + ' touché !');
		//ici on ajoute les trucs a faire quand la mite en parametre est touchée
		//par exemple, en attendant, la mite aura qu'une vie donc touchée = mort
		let idx = undefined;
		for (let i = 0; i < this.mites.length; i++) {
			if (this.mites[i] === mite) {
				this;
				idx = i;
			}
		}
		if (idx != undefined) {
			this.mites.splice(idx, 1);
		}
	}

	async doTir(mite) {
		if (
			this.shoots.length == 0 ||
			this.shoots[this.shoots.length - 1].cooldown == 1
		) {
			this.shoots.push(new tir(mite.miteX + 55, mite.miteY, 2, 0));
			this.shoots[this.shoots.length - 1].setCooldown();
		}
	}

	tirHandler() {
		if (this.shoots.length != 0) {
			for (let i = 0; i < this.shoots.length; i++) {
				if (this.shoots[i].x >= this.maxX) {
					this.shoots.splice(i, 1);
				} else {
					this.shoots[i].x = this.shoots[i].x + this.shoots[i].vitesse;
				}
			}
		}
	}

	shootTouchMonsters() {
		if (this.shoots.length != 0 && this.monsters.length != 0) {
			for (let index = 0; index < this.monsters.length; index++) {
				for (let i = 0; i < this.shoots.length; i++) {
					if (this.monsters.indexOf(this.monsters[index]) == index) {
						if (
							this.shoots[i].x >= this.monsters[index].monstreX - 55 &&
							this.shoots[i].x <= this.monsters[index].monstreX + 55
						) {
							if (
								this.shoots[i].y >= this.monsters[index].monstreY - 55 &&
								this.shoots[i].y <= this.monsters[index].monstreY + 55
							) {
								this.spawnBonus(
									this.monsters[index].monstreX,
									this.monsters[index].monstreY
								);
								this.monsters.splice(index, 1);
								this.shoots.splice(i, 1);
								this.score = this.score + 100;
							}
						}
					}
				}
			}
		}
	}

	miteTouchBonus() {
		if (this.bonus.length != 0 && this.mites.length != 0) {
			for (let index = 0; index < this.bonus.length; index++) {
				for (let i = 0; i < this.mites.length; i++) {
					if (this.monsters.indexOf(this.monsters[index]) == index) {
						if (
							this.bonus[index].x >= this.mites[i].miteX - 25 &&
							this.bonus[index].x <= this.mites[i].miteX + 25
						) {
							if (
								this.bonus[index].y >= this.mites[i].miteY - 60 &&
								this.bonus[index].y <= this.mites[i].miteY + 60
							) {
								this.activateBonus(this.bonus[index], this.mites[i]);
								this.bonus.splice(index, 1);
							}
						}
					}
				}
			}
		}
	}

	activateBonus(bonus, mite) {
		console.log(bonus);
		console.log(mite);
		if (bonus.type === 0) {
			// Immunité
			mite.giveImmunity();
			console.log('Immunité activée !');
		} else if (bonus.type === 1) {
			// Vitesse
			mite.speedUp();
			console.log('Vitesse augmentée !');
		}
	}
	dd;

	spawnBonus(x, y) {
		let random = Math.floor(Math.random() * 10);
		if (random === 1) {
			this.bonus.push(new bonus(x, y));
		}
	}

	replaceMite() {
		if (this.mites.length != 0) {
			for (let index = 0; index < this.mites.length; index++) {
				if (this.mites[index].miteX > this.maxX - 75) {
					this.mites[index].miteX = this.maxX - 75;
				}
				if (this.mites[index].miteX < 0) {
					this.mites[index].miteX = 0;
				}
				if (this.mites[index].miteY > this.maxY - 75) {
					this.mites[index].miteY = this.maxY - 75;
				}
				if (this.mites[index].miteY < 0) {
					this.mites[index].miteY = 0;
				}
			}
		}
	}

	/*
	actionMite(inputs, mite) {
		console.log(mite);
		for (let i = 0; i < inputs.length; i++) {
			if (inputs.at(0).isPressed == true) {
				mite.miteY = mite.getY() + 1 * mite.vitesse;
			}
			if (inputs.at(1).isPressed == true) {
				mite.miteY = mite.getY() - 1 * mite.vitesse;
			}
			if (inputs.at(2).isPressed == true) {
				mite.miteX = mite.getX() + 1 * mite.vitesse;
			}
			if (inputs.at(3).isPressed == true) {
				mite.miteX = mite.getX() - 1 * mite.vitesse;
			}
			if (inputs.at(4).isPressed == true) {
				this.doTir(mite);
			}
		}
	}*/

	getPlayerDown(data) {
		let mite = this.findMiteById(data.user);
		mite?.keyIsDown(data.key);
	}

	getPlayerUp(data) {
		let mite = this.findMiteById(data.user);
		mite?.keyIsUp(data.key);
	}

	difficultyUp() {
		if (this.vitesseMonster < 20) {
			this.vitesseMonster++;
		}
		if (this.apparitionMonster > 25000) {
			this.apparitionMonster = this.apparitionMonster - 3000;
		}
	}
}
