import assert from 'node:assert/strict';
import { describe, it } from 'node:test';
import Party from './Party.js';
import mite from './mite.js';
import tir from './tir.js';

describe('Party', () => {
	const partie = new Party(4);
	it('should create a new Party with a max of 4 players', () => {
		assert.deepStrictEqual(partie.players.playerArray.length, 0);
	});
	it('should add a player', () => {
		partie.addPlayer('Michel', new mite(1, 1), new tir());
		assert.deepStrictEqual(partie.players.playerArray.length, 1);
	});
	it('should not add the player 5', () => {
		partie.addPlayer('Pascal', new mite(1, 1), new tir());
		partie.addPlayer('Peter', new mite(1, 1), new tir());
		partie.addPlayer('Philippe', new mite(1, 1), new tir());
		partie.addPlayer('Fabien', new mite(1, 1), new tir());
		assert.deepStrictEqual(partie.players.playerArray.length, 4);
	});
	it('should return the pseudo of the last player', () => {
		assert.deepStrictEqual(partie.getPseudoOfAPlayer(4), 'Philippe');
	});
});
