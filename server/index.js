import http from 'http';
import express from 'express';
import addWebpackMiddleware from './middlewares/addWebpackMiddleware.js';
import { Server as IOServer } from 'socket.io';
import PlayerTab from './PlayerTab.js';
import Party from './Party.js';
import mite from './mite.js';
import tir from './tir.js';
import { ScoreSaver } from './ScoreSaver.js';
import Player from './Player.js';

const app = express();
const httpServer = http.createServer(app);
const io = new IOServer(httpServer);
const playerTab = new PlayerTab(4);
let partie = undefined;
const scoresSaver = new ScoreSaver();

//scoresSaver.saveScore({ pseudo: 'test', score: 1825 });

let port = process.env.PORT;
if (port == undefined) {
	port = '8000';
}

httpServer.listen(port, () => {
	console.log(`Server running at http://localhost:${port}/`);
});

io.on('connection', socket => {
	console.log(`${socket.id} s'est connecté`);
	console.log(socket);

	socket.on('disconnect', () => {
		console.log(`${socket.id} s'est déconnecté`);
		//playerTab.removePlayer(parseInt(sessionStorage.getItem('userId')));
		if (playerTab.length == 0) {
			this.partie == undefined;
		} else {
			partie.players = playerTab;
			io.emit('startGame', { partie: partie });
		}
	});

	/*
	socket.on('join', (pseudo, callback) => {
		let idPlayer = playerTab.addPlayer(pseudo);
		partie.players = playerTab;
		partie.mites.push(new mite(idPlayer));
		callback({ player: idPlayer });
		io.emit('newGame', { partie: partie });
	});*/

	socket.on('wantToPlay', (data, callback) => {
		if (partie === undefined) {
			partie = new Party(4, io, data.canvasWidth, data.canvasHeigth);
			let idPlayer = playerTab.addPlayer(data.pseudo);
			partie.players = playerTab;
			partie.mites.push(new mite(idPlayer));
			callback({ id: idPlayer });
			io.emit('startGame', { partie: partie });
		} else {
			let idPlayer = playerTab.addPlayer(data.pseudo);
			partie.players = playerTab;
			partie.mites.push(new mite(idPlayer));
			callback({ id: idPlayer });
			io.emit('startGame', { partie: partie });
		}
	});

	/*
	socket.on('settingsCanvas', settings => {
		partie.changeSettings(settings);
	});

	socket.on('updateGame', () => {
		setTimeout(() => {
			socket.emit('renderGame', { partie: partie });
		}, 1000 / 60);
		console.log('Update');
	});*/

	/*
	socket.on('action', infos => {
		partie.actionMite(infos.inputs, partie.findMiteById(infos.id));
	});*/

	socket.on('keyDown', data => {
		partie.getPlayerDown(data);
	});

	socket.on('keyUp', data => {
		partie.getPlayerUp(data);
	});

	socket.on('shoots', data => {
		partie.doTir(partie.findMiteById(data.user));
	});

	socket.on('changeGame', () => {
		partie = new Party(4);
		io.emit('newGame', { game: partie });
	});

	socket.on('wantHighScore', () => {
		socket.emit('highScore', scoresSaver.getScores());
	});
});

addWebpackMiddleware(app);
app.use(express.static('client/public'));

app.get('/:path', (req, res) => {
	res.redirect('/');
});
