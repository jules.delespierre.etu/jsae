import { readFileSync,writeFileSync } from 'node:fs';

export class ScoreSaver {
	path;

	constructor() {
		this.path = 'server/score.json';
	}

	getScores(){
		const scores = JSON.parse(readFileSync(this.path));
		return scores;
	}

	saveScore(data) {
		const scores = JSON.parse(readFileSync(this.path));

		let userScore = scores.find(element => element.pseudo == data.pseudo);
		if (userScore != null) {
			const index = scores.indexOf(userScore);

			scores.splice(index, 1);
			userScore = {
				pseudo: data.pseudo,
				date: new Date().toLocaleDateString('fr-FR'),
				score: data.score,
			};
			scores.push(userScore);
		} else {
			userScore = {
				pseudo: data.pseudo,
				date: new Date().toLocaleDateString('fr-FR'),
				score: data.score,
			};
			scores.push(userScore);
		}
		writeFileSync(this.path, JSON.stringify(scores));
	}
}
