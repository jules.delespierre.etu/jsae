export default class tir {
	x;
	y;
	vitesse;
	cooldown;

	constructor(x, y, vitesse, cooldown) {
		this.x = x;
		this.y = y;
		this.vitesse = vitesse;
		this.cooldown = cooldown;
	}

	sleep(ms) {
		return new Promise(resolve => setTimeout(resolve, ms));
	}

	async setCooldown() {
		await this.sleep(200);
		this.cooldown = 1;
	}
}
