import assert from 'node:assert/strict';
import { describe, it } from 'node:test';
import mite from './mite.js';

describe('Mite', () => {
	let m = new mite('the id');
	it('should return the X of the mite', () => {
		assert.deepStrictEqual(m.getX(), 0);
	});
	it('should return the X of the mite', () => {
		assert.deepStrictEqual(m.getY(), 0);
	});

	it('should return the speed of the mite', () => {
		assert.deepStrictEqual(m.getVitesse(), 1);
	});
	it('should return amount of life of the mite', () => {
		assert.deepStrictEqual(m.getVies(), 3);
	});
	it('should return amount of life of the mite', () => {
		assert.deepStrictEqual(m.getVies(), 3);
	});
	it('should move the mite on the bottom', () => {
		m.down = true;
		m.actionMite();
		m.down = false;
		assert.deepStrictEqual(m.getY(), 1);
	});
	it('should move the mite on the top', () => {
		m.up = true;
		m.actionMite();
		m.up = false;
		assert.deepStrictEqual(m.getY(), 0);
	});
	it('should move the mite on the right', () => {
		m.right = true;
		m.actionMite();
		m.right = false;
		assert.deepStrictEqual(m.getX(), 1);
	});
	it('should move the mite on the left', () => {
		m.left = true;
		m.actionMite();
		m.left = false;
		assert.deepStrictEqual(m.getX(), 0);
	});
});
