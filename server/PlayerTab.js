import Player from './Player.js';

export default class PlayerTab {
	playerArray;
	limit;
	constructor(limit) {
		this.playerArray = new Array();
		this.limit = limit;
	}

	addPlayer(pseudo) {
		if (this.playerArray.length == 0) {
			this.playerArray.push(new Player(1, pseudo));
			return this.playerArray.at(0).id;
		} else {
			this.playerArray.push(new Player(this.playerArray.length + 1, pseudo));
			return this.playerArray.at(this.playerArray.length - 1).id;
		}
	}

	removePlayer(idPlayer) {
		if (this.playerArray.length != 0) {
			this.playerArray.forEach(element => {
				if (element.id == idPlayer) {
					this.playerArray.splice(this.playerArray.indexOf(element), 1);
				}
			});
		}
	}
}
