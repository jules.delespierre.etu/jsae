import http from 'http';
import express from 'express';
import addWebpackMiddleware from './middlewares/addWebpackMiddleware.js';
import { Server as IOServer } from 'socket.io';
import Party from './Party.js';

const app = express();
addWebpackMiddleware(app);
const partie = new Party(4);
const httpServer = http.createServer(app);
const playerTab = new PlayerTab(4);
