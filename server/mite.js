export default class mite {
	vitesse;
	miteX;
	miteY;
	vies;
	immunity;
	idPlayer;

	constructor(idPlayer) {
		this.vitesse = 1;
		this.miteX = 0;
		this.miteY = 0;
		this.vies = 3;
		this.immunity = false;
		this.idPlayer = idPlayer;
		this.down = false;
		this.up = false;
		this.left = false;
		this.right = false;
		setInterval(this.actionMite.bind(this), 0);
	}

	getX() {
		return this.miteX;
	}

	getY() {
		return this.miteY;
	}

	getVitesse() {
		return this.vitesse;
	}

	getImmunity() {
		return this.immunity;
	}

	getVies() {
		return this.vies;
	}

	replace() {
		if (this.getY() < 0) {
			this.miteY = 0;
		}
		if (this.getX() < 0) {
			this.miteX = 0;
		}
		if (this.getX() > canvas.clientWidth - miteImage.width) {
			this.miteX = canvas.clientWidth - miteImage.width;
		}
		if (this.getY() > canvas.clientHeight - miteImage.height) {
			this.miteY = canvas.clientHeight - miteImage.height;
		}
	}

	actionMite() {
		if (this.down) {
			this.miteY = this.getY() + 1 * this.vitesse;
		}
		if (this.up) {
			this.miteY = this.getY() - 1 * this.vitesse;
		}
		if (this.right) {
			this.miteX = this.getX() + 1 * this.vitesse;
		}
		if (this.left) {
			this.miteX = this.getX() - 1 * this.vitesse;
		}
	}

	keyIsDown(key) {
		if (key === 's') {
			this.down = true;
		}
		if (key === 'z') {
			this.up = true;
		}
		if (key === 'd') {
			this.right = true;
		}
		if (key === 'q') {
			this.left = true;
		}
	}

	keyIsUp(key) {
		if (key === 's') {
			this.down = false;
		}
		if (key === 'z') {
			this.up = false;
		}
		if (key === 'd') {
			this.right = false;
		}
		if (key === 'q') {
			this.left = false;
		}
	}

	speedUp() {
		this.vitesse = 2;
		setTimeout(() => {
			this.vitesse = 1;
		}, 5000);
	}

	addLife() {
		this.vies++;
	}

	giveImmunity() {
		this.immunity = true;
		setTimeout(() => {
			this.immunity = false;
			console.log('Immunity desactivated');
		}, 5000);
	}
}
