import tir from './tir.js';
import input from './input.js';
import monstre from './monstre.js';
import mite from './mite.js';
import score from './score.js';

let player1 = new mite();

let score = new score();

const canvas = document.querySelector('.game'),
	context = canvas.getContext('2d');

const miteImage = new Image();
miteImage.src = '/res/assets/mothSpriteShett.png';
miteImage.addEventListener('load', () => {
	setInterval(requestAnimationFrame(render), 1000 / 60);
});

const marron = new Image();
marron.src = '/res/assets/marron.png';
marron.addEventListener('load', () => {
	setInterval(requestAnimationFrame(render), 1000 / 60);
});

const monstreImage = new Image();
monstreImage.src = '/res/assets/monstre.png';
monstreImage.addEventListener('load', () => {
	setInterval(requestAnimationFrame(render), 1000 / 60);
});

setInterval(() => {
	spawnMonster();
}, 60000 / 60);

const inputPressed = [];
inputPressed.push(
	new input('down', false),
	new input('up', false),
	new input('right', false),
	new input('left', false),
	new input('shoot', false)
);

document.addEventListener('keydown', e => {
	if (e.key == 's') {
		inputPressed.at(0).setPressed(true);
	}
	if (e.key == 'z') {
		inputPressed.at(1).setPressed(true);
	}
	if (e.key == 'd') {
		inputPressed.at(2).setPressed(true);
	}
	if (e.key == 'q') {
		inputPressed.at(3).setPressed(true);
	}
	if (e.key == ' ') {
		inputPressed.at(4).setPressed(true);
	}

	for (let i = 0; i < inputPressed.length; i++) {
		if (inputPressed.at(0).isPressed == true) {
			player1.miteY = player1.getY() + 1 * player1.vitesse;
		}
		if (inputPressed.at(1).isPressed == true) {
			player1.miteY = player1.getY() - 1 * player1.vitesse;
		}
		if (inputPressed.at(2).isPressed == true) {
			player1.miteX = player1.getX() + 1 * player1.vitesse;
		}
		if (inputPressed.at(3).isPressed == true) {
			player1.miteX = player1.getX() - 1 * player1.vitesse;
		}
		if (inputPressed.at(4).isPressed == true) {
			doTir();
		}
	}
});

document.addEventListener('keyup', e => {
	if (e.key == 's') {
		inputPressed.at(0).setPressed(false);
	}
	if (e.key == 'z') {
		inputPressed.at(1).setPressed(false);
	}
	if (e.key == 'd') {
		inputPressed.at(2).setPressed(false);
	}
	if (e.key == 'q') {
		inputPressed.at(3).setPressed(false);
	}
	if (e.key == ' ') {
		inputPressed.at(4).setPressed(false);
	}
});

function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

function replace() {
	if (player1.getY() < 0) {
		player1.miteY = 0;
	}
	if (player1.getX() < 0) {
		player1.miteX = 0;
	}
	if (player1.getX() > canvas.clientWidth - miteImage.width) {
		player1.miteX = canvas.clientWidth - miteImage.width;
	}
	if (player1.getY() > canvas.clientHeight - miteImage.height) {
		player1.miteY = canvas.clientHeight - miteImage.height;
	}
}

function render() {
	context.clearRect(0, 0, canvas.clientWidth, canvas.clientHeight);
	replace();
	context.drawImage(miteImage, player1.getX(), player1.getY());
	tirHandler();
	renderMarron();
	moveMonster();
	renderMonster();
	shootTouchMonsters();
	ifMonsterTouchTheMite();
	requestAnimationFrame(render);
}

const canvasResizeObserver = new ResizeObserver(() => resampleCanvas());
canvasResizeObserver.observe(canvas);

function resampleCanvas() {
	canvas.width = canvas.clientWidth;
	canvas.height = canvas.clientHeight;
}

const tirs = [];
async function doTir() {
	if (tirs.length == 0 || tirs[tirs.length - 1].cooldown == 1) {
		let nouveauTir = new tir(player1.getX() + 75, player1.getY(), 5, 0);
		tirs.push(nouveauTir);
		tirs[tirs.length - 1].setCooldown();
		console.log(tirs);
	}
}

function tirHandler() {
	if (tirs.length != 0) {
		for (let i = 0; i < tirs.length; i++) {
			if (tirs[i].x >= canvas.width) {
				tirs.splice(i, 1);
			} else {
				tirs[i].x = tirs[i].x + tirs[i].vitesse;
			}
		}
	}
}

function renderMarron() {
	if (tirs.length != 0) {
		for (let i = 0; i < tirs.length; i++) {
			context.drawImage(marron, tirs[i].x, tirs[i].y);
		}
	}
}

const monstres = [];

function spawnMonster() {
	let nouveauMonstre = new monstre(
		canvas.clientWidth,
		Math.floor(Math.random() * canvas.height),
		2
	);
	monstres.push(nouveauMonstre);
}

function moveMonster() {
	if (monstre.length != 0) {
		for (let index = 0; index < monstres.length; index++) {
			if (monstres[index].x <= 0) {
				monstres.splice(index, 1);
			} else {
				monstres[index].x = monstres[index].x - monstres[index].vitesse;
			}
		}
	}
}

function renderMonster() {
	if (monstres.length != 0) {
		for (let i = 0; i < monstres.length; i++) {
			context.drawImage(monstreImage, monstres[i].x, monstres[i].y);
		}
	}
}

function shootTouchMonsters() {
	console.log(tirs.length);
	if (tirs.length != 0 && monstres.length != 0) {
		for (let index = 0; index < monstres.length; index++) {
			for (let i = 0; i < tirs.length; i++) {
				if (monstres.indexOf(monstres[index]) == index) {
					if (
						tirs[i].x >= monstres[index].x - monstreImage.width &&
						tirs[i].x <= monstres[index].x + monstreImage.width
					) {
						if (
							tirs[i].y >= monstres[index].y - monstreImage.height &&
							tirs[i].y <= monstres[index].y + monstreImage.height
						) {
							monstres.splice(index, 1);
							tirs.splice(i, 1);
						}
					}
				}
			}
		}
	}
}

function ifMonsterTouchTheMite() {
	for (let i = 0; i < monstres.length; i++) {
		if (
			player1.getX() - miteImage.width <= monstres[i].x - monstreImage.width &&
			player1.getX() + miteImage.width >= monstres[i].x + monstreImage.width
		) {
			if (
				player1.getY() - miteImage.height <=
					monstres[i].y - monstreImage.height &&
				player1.getY() + miteImage.height >= monstres[i].y + monstreImage.height
			) {
				console.log(player1.getVies());
				player1.vie--;
				if (player1.getVies() == 0) {
					gameOver();
				} else {
					immunity();
				}
			}
		}
	}
}

function gameOver() {
	console.log('perdu');
}

function immunity() {
	player1.immunity = true;
	setTimeout(() => {
		player1.immunity = false;
	}, 5000);
}
