export default class score {
	score;

	constructor() {
		this.score = 0;
	}

	updateScore(nbPoints) {
		this.score += nbPoints;
	}
}
